;;; super-duper-yes-or-no.el --- A "better"(?) yes-or-no-p. -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuval Langer

;; Author: Yuval Langer <yuval.langer@gmail.com>
;; Version: 1.0.0
;; Keywords:
;; URL: https://sr.ht/~kakafarm/super-duper-yes-or-no/

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage:
;;
;; To replace yes-or-no-p with random sequences of words:
;;
;; (defalias 'yes-or-no-p
;;           'super-duper-yes-or-no-yes-or-no-words-p)
;;
;; To replace yes-or-no-p with uppercase letters hunts:
;;
;; (defalias 'yes-or-no-p
;;           'super-duper-yes-or-no-yes-or-no-toggle-case-p)
;;
;; To replace yes-or-no-p with arithmetic problems:
;;
;; (defalias 'yes-or-no-p
;;           'super-duper-yes-or-no-yes-or-no-arithmetic-problem-p)

;;; Code:

(require 'cl-lib)

(defcustom sd-number-of-words 2
  "Number of words in the `sd-yes-or-no-words-p' words sequences."
  :type 'natnum
  :group 'super-duper-yes-or-no)

(defvar sd-words
  ["aaaaa"
   "moo"
   "foo"
   "bar"]
  "Words to be used by `sd-yes-or-no-words-p'.

The default vector here is just an example - you could replace it
with a longer and better vector of words, like one of the
Diceware wordlists (https://diceware.dmuth.org/), or one of the
EFF wordlists (https://www.eff.org/dice).")

(defcustom sd-upper-case-phrase-for-yes
  "(If you understand what you're doing, enter the capitalized letters here for \"yes\")"
  "Phrase to be used by the \"yes\" prong of `sd-yes-or-no-toggle-case-p'."
  :type 'string
  :group 'super-duper-yes-or-no)

(defcustom sd-upper-case-phrase-for-no
  "(If you understand what you're doing, enter the capitalized letters here for \"no\")"
  "Phrase to be used by the \"no\" prong of `sd-yes-or-no-toggle-case-p'."
  :type 'string
  :group 'super-duper-yes-or-no)

(defcustom sd-number-of-case-toggle-characters 4
  "Number of English ASCII chars toggled in a phrase.

This number is used in `sd-yes-or-no-toggle-case-p'."
  :type 'natnum
  :group 'super-duper-yes-or-no)

(defcustom sd-arithmetic-problem-minimum
  0
  "Smallest integer to be used in `sd-yes-or-no-arithmetic-problem-p'."
  :type 'integer
  :group 'super-duper-yes-or-no)

(defcustom sd-arithmetic-problem-maximum
  10
  "Biggest integer to be used in `sd-yes-or-no-arithmetic-problem-p'."
  :type 'integer
  :group 'super-duper-yes-or-no)

(defcustom sd-arithmetic-problem-template
  '(+ (* 0 0)
      (* 0 0))
  "Template used to create arithmetic problems.

Used by `sd-yes-or-no-arithmetic-problem-p'.

The atomic arguments are replaced by integers and the functions
stay unchanged."
  :type 'list
  :group 'super-duper-yes-or-no)

(defcustom sd-two-prongs nil
  "If t, ask for two hard to enter inputs, otherwise ask only for the affermative."
  :type 'bool
  :group 'super-duper-yes-or-no)

(defun sd--randint (minimum maximum)
  "Return an integer between MINIMUM (inclusive) and MAXIMUM (exclusive)."

  (+ minimum
     (random (- maximum
                minimum))))

(defun sd--make-random-input-string (wanted-number-of-words)
  "Make a list of words, WANTED-NUMBER-OF-WORDS long.

Used in the `sd-yes-or-no-words-p' function."
  (cl-loop
   with wordlist-size =
   (seq-length sd-words)

   for random-word-index =
   (random wordlist-size)

   for random-word =
   (aref sd-words
         random-word-index)

   repeat wanted-number-of-words

   collect random-word))

(defun sd--make-random-yes-or-no-input-pair (number-of-words)
  "Make two different word lists, each NUMBER-OF-WORDS long.

Used in super-duper-yes-or-no-words-p function."
  (cl-loop
   with yes-input =
   (sd--make-random-input-string
    number-of-words)

   for no-input =
   (sd--make-random-input-string
    number-of-words)

   while
   (equal yes-input
          no-input)

   finally return
   (list yes-input
         no-input)))

(defun sd--list-intersperse (input-list intersperser)
  "Return INPUT-LIST interspersed with INTERSPERSER.

Examples:

\\='(sd--list-intersperse \\='() \\='d) returns \\='()
\\='(sd--list-intersperse \\='(a) \\='d) returns \\='(a)
\\='(sd--list-intersperse \\='(a b) \\='d) returns \\='(a d b)
\\='(sd--list-intersperse \\='(a b c) \\='d) returns \\='(a d b d c)"
  (if (null input-list)
        '()
    (cons (car input-list)
          (cl-loop
           for item
           in (cdr input-list)

           append
           (list intersperser item)))))

(defun sd-yes-or-no-words-p (prompt)
  "Ask user a yes or no question.

Display in minibuffer PROMPT followed by a sequence of words the
user must enter to choose yes.

If the variable `sd-two-prongs' is t, ask two sequences, one for
the affermative and one for the negative."
  (cl-loop
   for wanted-yes-or-no =
   (super-duper-yes-or-no--make-random-yes-or-no-input-pair
    super-duper-yes-or-no-number-of-words)

   for wanted-yes =
   (car wanted-yes-or-no)

   for wanted-no =
   (if sd-two-prongs
       (cadr wanted-yes-or-no)
     (list "no"))

   for wanted-yes-string =
   (apply 'concat
          (super-duper-yes-or-no--list-intersperse
           wanted-yes
           " "))

   for wanted-no-string =
   (apply 'concat
          (super-duper-yes-or-no--list-intersperse
           wanted-no
           " "))

   for user-input =
   (read-from-minibuffer
    (concat prompt
            "(Enter \""
            wanted-yes-string
            "\" for yes, \""
            wanted-no-string
            "\" for no) "))

   until
   (or (equal user-input
              wanted-yes-string)
       (equal user-input
              wanted-no-string))

   finally return
   (equal user-input
          wanted-yes-string)))

(defun sd--toggle-char-case (our-char)
  "Return the opposite case of OUR-CHAR.

Examples:

- (sd--toggle-char-case ?a) returns ?A
- (sd--toggle-char-case ?B) returns ?b"
  (cond
   ((char-uppercase-p our-char)
    (downcase our-char))
   (t
    (upcase our-char))))

(defun sd--randomly-toggle-string-case (input-string number-of-chars-to-toggle)
  "Toggle a number of chars' case in a string.

A NUMBER-OF-CHARS-TO-TOGGLE chars will be selected at random from
INPUT-STRING.  Then function returns INPUT-STRING, with the
selected chars' case toggled.

NUMBERS-OF-CHARS-TO-TOGGLE MUST be less or equal to the number of
English ASCII chars in INPUT-STRING - other chars do not count in
the toggle count AND they are not toggled."
  (let* ((input-length (length input-string))
         (our-shuffled-positions
          (cl-loop
           with our-positions =
           (number-sequence 0 (1- input-length))

           for wanted-position-index =
           (random (length our-positions))

           for wanted-position =
           (nth wanted-position-index
                our-positions)

           for position-char =
           (aref input-string
                 wanted-position)

           when
           (or (<= ?a
                   position-char
                   ?z)
               (<= ?A
                   position-char
                   ?Z))

           do
           (setq our-positions
                 (delq wanted-position
                       our-positions))

           repeat number-of-chars-to-toggle

           collect wanted-position)))

    (concat
     (cl-loop
      for input-char
      across input-string

      for input-string-position
      in (number-sequence 0
                          (1- input-length))

      collect
      (if (member
           input-string-position
           our-shuffled-positions)
          (sd--toggle-char-case input-char)
        input-char)))))

(defun sd--string-only-uppercase (input-string)
  "Return INPUT-STRING with only the uppercase chars."
  (cl-loop
   for input-char
   across input-string
   when (char-uppercase-p input-char)
   concat (char-to-string input-char)))

(defun sd-yes-or-no-toggle-case-p (prompt)
  "Ask user a yes or no question, but expect the uppercase letters.

Display in minibuffer PROMPT followed by a string with some of
its letters' case flipped.  Enter the uppercase letters found in
sequence for the affermative and \"no\" for the negative.

If `sd-two-prongs' is t, provides the user with two of such
strings.  Enter the second one's uppercase letters for the
negative."
  (cl-loop
   for wanted-yes-prompt =
   (sd--randomly-toggle-string-case
    sd-upper-case-phrase-for-yes
    sd-number-of-case-toggle-characters)

   for wanted-no-prompt =
   (if sd-two-prongs
       (sd--randomly-toggle-string-case
        sd-upper-case-phrase-for-no
        sd-number-of-case-toggle-characters)
     "(Enter \"no\" for no)")

   for wanted-input-yes =
   (sd--string-only-uppercase
    wanted-yes-prompt)

   for wanted-input-no =
   (if sd-two-prongs
       (sd--string-only-uppercase
        wanted-no-prompt)
     "no")

   while
   (equal wanted-input-yes
          wanted-input-no)

   finally return
   (cl-loop
    for user-input =
    (read-from-minibuffer
     (concat prompt
             wanted-yes-prompt
             "\n"
             wanted-no-prompt
             ":\n"))

    until (or (equal user-input
                     wanted-input-yes)
              (equal user-input
                     wanted-input-no))

    finally return
    (equal user-input
           wanted-input-yes))))

(defun sd--make-arithmetic-problem-number ()
  "Return a random integer number between the two variables.
`sd-arithmetic-problem-minimum' and `sd-arithmetic-problem-maximum'."
  (sd--randint sd-arithmetic-problem-minimum
               (1+ sd-arithmetic-problem-maximum)))

(defun sd--make-arithmetic-problem-arguments (argument-list)
  "Return ARGUMENT-LIST in an arithmetic problem with actual integer values."
  (cl-loop
   for argument
   in argument-list

   collect
   (if (atom argument)
       (sd--make-arithmetic-problem-number)
     (sd--make-arithmetic-problem
      argument))))

(defun sd--make-arithmetic-problem (arithmetic-expression-template)
  "Make an arithmetic problem using an ARITHMETIC-EXPRESSION-TEMPLATE."
  (cond
   ((null arithmetic-expression-template) '())
   (t (cons (car arithmetic-expression-template)
            (sd--make-arithmetic-problem-arguments
             (cdr arithmetic-expression-template))))))

(defun sd-yes-or-no-arithmetic-problem-p (prompt)
  "Ask user a yes or no question, but as an answer to an arithmetic expression.

Display in minibuffer PROMPT followed by an arithmetic
expression.  Enter the answer to the expression for affermitive
and \"no\" for the negative.

If `sd-two-prongs' us t, two arithmetic expressions appear,
enter the second's result for the negative."
  (cl-loop
   for wanted-yes-prompt =
   (sd--make-arithmetic-problem
    sd-arithmetic-problem-template)

   for wanted-no-prompt =
   (if sd-two-prongs
       (sd--make-arithmetic-problem
        sd-arithmetic-problem-template)
     "no")

   for wanted-input-yes =
   (number-to-string (eval wanted-yes-prompt))

   for wanted-input-no =
   (if sd-two-prongs
       (number-to-string (eval wanted-no-prompt))
     "no")

   while
   (equal wanted-input-yes
          wanted-input-no)

   finally return
   (cl-loop
    for user-input =

    (read-from-minibuffer
     (concat prompt
             "Please answer "
             (format "%S" wanted-yes-prompt)
             " for \"yes\" and "
             (format "%S" wanted-no-prompt)
             " for \"no\": "
             (format "%s" (list wanted-input-yes
                                wanted-input-no))))

    until
    (or (equal user-input
               wanted-input-yes)
        (equal user-input
               wanted-input-no))

    finally return
    (equal user-input
           wanted-input-yes))))

(provide 'super-duper-yes-or-no)

;; Local Variables:
;; read-symbol-shorthands: (("sd-" . "super-duper-yes-or-no-"))
;; End:
;;; super-duper-yes-or-no.el ends here
